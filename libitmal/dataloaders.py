from keras.datasets import mnist
import matplotlib.pyplot as plt

def MNIST_PlotDigit(data):
    image = data.reshape(28, 28)
    plt.imshow(image, cmap='gray_r')
    plt.show()

def MNIST_GetDataSet(All=False):
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    if All:
        return X_train, y_train, X_test, y_test
    #else:
     #   return X, y = np.concatenate((X_train, X_test)), np.concatenate((y_train, y_test))
    